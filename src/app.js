var rabbitmq_host = process.env.RABBITMQ_HOST || "localhost";
var rabbitmq_port = process.env.RABBITMQ_PORT || "5672";
var rabbitmq_user = process.env.RABBITMQ_USER || "guest";
var rabbitmq_password = process.env.RABBITMQ_PASSWORD || "guest";
var queue = process.env.QUEUE || "hello";
var message = process.env.MESSAGE || "Hello World!";

var amqp = require('amqplib/callback_api');
amqp.connect('amqp://'+ rabbitmq_user + ':' + rabbitmq_password +'@' + rabbitmq_host + ':' + rabbitmq_port, (conErr, connection) => {
    // Catch connection errors
    if (conErr) { throw conErr; }

    connection.createChannel((chErr, channel) => {
	// Creation of queue is idempotent
	channel.assertQueue(queue, { durable: false });

	channel.sendToQueue(queue, Buffer.from(message));
	console.log(" [x] Sent %s", message);

	// Close the connection
        setTimeout(() => {
          connection.close();
          process.exit(0)
          }, 500);
    });
});


FROM node:latest

COPY src/ /home/node
WORKDIR /home/node

USER node

CMD ["node", "app.js"]

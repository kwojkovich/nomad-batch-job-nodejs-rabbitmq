job "send-msg-rabbitmq" {
  datacenters = ["dc1"]
  type = "batch"
  parameterized {
    payload = "forbidden"
    meta_required = ["queue", "message"]
  }

  group "cache" {

    task "send-msg" {
      driver = "docker"

      config {
        image = "docker.kwojo314.com/nodejs-rabbitmq-example:0.0.2"
        auth {
          username = "redacted"
          password = "redacted"
        }
      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB

        network {
          mbits = 10
        }
      }

      template {
        data        = <<EOH
RABBITMQ_USER="admin"
QUEUE="{{ env "NOMAD_META_queue" }}"
MESSAGE="{{ env "NOMAD_META_message" }}"
{{ with secret "production/data/rabbitmq" }}
RABBITMQ_PASSWORD="{{ .Data.data.RABBITMQ_ADMIN_PW | toJSON }}"
{{ end }}
EOH
        destination = "local/config.env"
        env         = true
      }

      vault {
        policies      = ["production"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }
    }
  }
}

# nodejs-rabbitmq-example 
This is a demonstration of a Parameterized Batch Nomad job to do a small amount of work.
In this case a simple NodeJS application sends a message to RabbitMQ using parameters defined on the CLI.

# Usage
These are the steps that I use to run on my personal Nomad + Consul + Vault cluster. You may need to make
changes to accomodate your setup.

## Clone this repo
```
git clone git@gitlab.com:kwojkovich/nomad-batch-job-nodejs-rabbitmq.git
```

## Pull in npm dependancies

```
cd src
npm install
cd ../
```

## Build the docker image

```
docker login <your_docker_repo>
docker build -t <your_docker_repo>/<image_name>:<tag> .
docker push <your_docker_repo>/<image_name>:<tag>
```

## Modify the Nomad Job Specification

Edit `nodejs-send-msg-rabbitmq.nomad` to suit your needs.

You will likely need to edit:

 * `job > datacenters`
 * `job > group > task > config > image`
 * `job > group > task > config > auth`
 * `job > group > task > template`
 * `job > group > task > vault > policies`

## Submit the job
Submit the job to Nomad. I like using `nomad plan` to make sure I have enough resources and that there are no obvious errors.

```
nomad plan nodejs-send-msg-rabbitmq.nomad
<use the `nomad run` output from the plan command.
```

## Run the a batch job

```
nomad job dispatch -meta 'message=Hello from nomad!' -meta 'queue=hello' send-msg-rabbitmq
```

The output will look like:

```
nomad job dispatch -meta 'message=Hello from nomad!' -meta 'queue=hello' send-msg-rabbitmq
Dispatched Job ID = send-msg-rabbitmq/dispatch-1599063610-c45d405a
Evaluation ID     = 5e8530bc

==> Monitoring evaluation "5e8530bc"
    Evaluation triggered by job "send-msg-rabbitmq/dispatch-1599063610-c45d405a"
    Allocation "8226b3ab" created: node "9f227a69", group "cache"
    Evaluation status changed: "pending" -> "complete"
==> Evaluation "5e8530bc" finished with status "complete"
```

Take note of the `Allocation ____` value. In this example we will use "8226b3ab", replace this value with your allocation ID.

## Monitor the job

While the job is running, I like to monitor the output. This uses the `watch` command to refresh the status every 1 second.

```
watch -n 1 nomad alloc status 8226b3ab
```


## Review output

Review `stdout`:
```
nomad logs 8226b3ab
```

Review `stderr`:

```
nomad logs 8226b3ab
```

## Check RabbitMQ

You should now have messages awaiting in your RabbitMQ cluster.
